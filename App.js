/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {useState} from 'react';
import HomeScreen from 'views/Home';
import AugustoHomepageScreen from 'views/AugustoHomepage';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const {Navigator, Screen} = createStackNavigator();
const App: () => React$Node = () => {
  return (
    <NavigationContainer>
      <Navigator initialRouteName="Home">
        <Screen
          name={'Home'}
          component={HomeScreen}
          options={{headerShown: false}}
        />
        <Screen
          name={'AugustoHomepage'}
          component={AugustoHomepageScreen}
          options={{title: 'Augusto Digital'}}
        />
      </Navigator>
    </NavigationContainer>
  );
};

export default App;
