import {View} from 'react-native';
export const createAppContainer = jest
  .fn()
  .mockReturnValue(function NavigationContainer(props) {
    return null;
  });
export const createDrawerNavigator = jest.fn();
export const createMaterialTopTabNavigator = jest.fn();
export const createBottomTabNavigator = jest.fn();
export const createStackNavigator = jest
  .fn()
  .mockImplementation(x => ({Navigator: View, Screen: View}));
export const createSwitchNavigator = jest.fn();
export const Header = {
  HEIGHT: 22,
  WIDTH: 800,
};
export const StackActions = {
  push: jest.fn().mockImplementation(x => ({...x, type: 'Navigation/PUSH'})),
  replace: jest
    .fn()
    .mockImplementation(x => ({...x, type: 'Navigation/REPLACE'})),
  reset: jest.fn().mockImplementation(x => ({...x, type: 'Navigation/RESET'})),
};
export const NavigationActions = {
  navigate: jest.fn().mockImplementation(x => x),
};
