module.exports = {
  root: true,
  extends: ['@react-native-community'],
  plugins: ['react', 'react-native', 'jest', 'detox'],
  env: {
    'jest/globals': true,
    'detox/detox': true,
  },
};
