describe('Example', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should have welcome screen', async () => {
    await expect(element(by.id('home-screen-view'))).toBeVisible();
  });

  it('should increment the counter after tapping the button', async () => {
    await expect(element(by.id('counter'))).toHaveText('0');
    await element(by.id('just-a-button')).tap();
    await element(by.id('just-a-button')).tap();
    await element(by.id('just-a-button')).tap();
    await expect(element(by.id('counter'))).toHaveText('3');
  });
});
