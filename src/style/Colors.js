export const Colors = {
  lighter: '#ccc',
  white: '#fff',
  black: '#000',
  red: '#b22',
  green: '#8b5',
  blue: 'rgb(19, 46, 63)',
};

export default Colors;
