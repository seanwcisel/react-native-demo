import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
} from 'react-native';

import styles from './styles';
import {Colors} from 'style/Colors';
import {useNavigation} from '@react-navigation/native';

const Home: () => React$Node = () => {
  const [counter, setCounter] = useState(0);
  const navigation = useNavigation();

  const incrementCounter = () => setCounter(counter + 1);

  const handleNavigateButtonTap = () => navigation.push('AugustoHomepage');

  return (
    <View>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView testID={'home-screen-view'}>
        <View
          style={styles.headerContainer}
          testID={'header-container-element'}
        >
          <Text style={styles.sectionTitle}>React Native Demo</Text>
        </View>

        <View style={styles.body}>
          <Text style={styles.largeText} testID="counter">
            {counter}
          </Text>
          <Button
            title="Tap me"
            testID={'just-a-button'}
            onPress={incrementCounter}
            color={Colors.green}
          />
          <Button
            title="Navigate"
            testID={'just-a-nav-button'}
            onPress={handleNavigateButtonTap}
            color={Colors.green}
          />
        </View>
      </SafeAreaView>
    </View>
  );
};

export default Home;
