import {StyleSheet} from 'react-native';
import {Colors} from 'style';
export default StyleSheet.create({
  body: {
    justifyContent: 'center',
    height: '100%',
  },
  largeText: {
    alignSelf: 'center',
    fontSize: 30,
    fontWeight: '700',
  },
  headerContainer: {
    padding: 32,
    backgroundColor: Colors.blue,
  },
  buttonLabel: {
    padding: 20,
    backgroundColor: Colors.green,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.lighter,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
});
