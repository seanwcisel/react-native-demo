/**
 * @format
 */

import 'react-native';
import React from 'react';
import Home from '../';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
jest.mock('@react-navigation/native');

it('renders correctly', () => {
  renderer.create(<Home />);
});

it('matches snapshot', () => {
  const instance = renderer.create(<Home />);
  expect(instance.toJSON()).toMatchSnapshot();
});
