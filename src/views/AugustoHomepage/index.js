import React, {useState} from 'react';
import {View} from 'react-native';

import {Colors} from 'style/Colors';

import {WebView} from 'react-native-webview';

const Home: () => React$Node = () => {
  return (
    <View style={{flex: 1}}>
      <WebView source={{uri: 'https://www.augustodigital.com/'}} />
    </View>
  );
};

export default Home;
